import os
import io
import boto3
import json
import csv
import urllib.request
from urllib.parse import urlparse


ENDPOINT_NAME = os.environ['ENDPOINT_NAME']
runtime= boto3.client('runtime.sagemaker')

def lambda_handler(event, context):
    
    data = json.loads(json.dumps(event))
    url = data["url"]
    
    file_name, headers = urllib.request.urlretrieve(url)
    
    
    with open(file_name, 'rb') as f:
        payload = f.read()
        payload = bytearray(payload)
    
    response = runtime.invoke_endpoint(EndpointName=ENDPOINT_NAME, 
                                       ContentType='image/jpeg', 
                                       Body=payload)
    result = response['Body'].read()
    inference = json.loads(result)
    
    if inference["valid"]:
        result = {"redirect_to_blocks": ["Valid Path"]}
    else:
        result = {"redirect_to_blocks": ["Invalid Path"]}
    
    return result